#!/bin/bash
set -euo pipefail

# Read DNS names from a file, examine the TLS certificates
# for the related sites and find the expiry dates of each.
# Return a list sorted such that the closest date is listed
# first.
#
# This script requires openssl, nmap and nmap-common to be installed.


usage() {
    echo -e "$0 <filename>\n"
    echo -e "\tRead a list of DNS names from an input file
\tand show a list of sorted TLS certificate end dates
\talong with the supported TLS/SSL versions."
}
if [ $# != 1 ]; then
    usage
    exit 1
fi

INPUT=$1

if [ ! -f ${INPUT} ]; then
    echo -e "${INPUT} does not exist!"
    exit 1
fi

echo -e "\tEnd Date\t\tName\t\t\tTLS/SSL Versions\n\t--------\t\t----\t\t\t----------------"
cat ${INPUT} | while read NAME
do
    # echo "# Processing ${NAME} ..."
    DATE=`openssl s_client -servername "${NAME}" \
        -connect "${NAME}:443" 2>/dev/null < /dev/null | \
        openssl x509 -noout -dates |egrep notAfter |cut -d'=' -f2`
    TLS_VERS=`nmap --script ssl-enum-ciphers -p 443 ${NAME}| \
        egrep "(SSL|TLS)v[0-9\.]+:" |cut -c5- |tr '\n:' ' '`
    echo -e "${DATE}|${NAME}|${TLS_VERS}" 
done | sort -k4n,4 -k1M,1 -k2n,2 -k3n,3 |column -t -s'|'