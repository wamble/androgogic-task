# Bash Scripting Task


Create a bash script to find the TLS certificate expiry date for the remote servers listed in a text file, along with an assessment on what TLS versions these sites support (SSLv3, TLS1.0, TLS1.1, TLS1.2).  Sort the resulting list by which certificate expires first.

Example text file (sitelist.txt) for input:

	www.google.com.au
	leo.acu.edu.au
	lnlcms.lexisnexis.com.au
